var EQ_summaryInfo = [
{label:"high coupling, high complexity, low cohesion", color: "#E50000", value: 36500,count:49},
{label:"high coupling, high complexity", color: "#FF5B13", value: 10276,count:34},
{label:"high coupling", color: "#FFC800", value: 99,count:1},
{label:"high complexity", color: "#62BF18", value: 21365,count:206},
{label:"fair quality attributes", color: "#007F24", value: 27246,count:640}];
