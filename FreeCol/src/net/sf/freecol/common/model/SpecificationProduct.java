/**
 * Copyright (C) 2002-2015   The FreeCol Team This file is part of FreeCol. FreeCol is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version. FreeCol is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with FreeCol.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.sf.freecol.common.model;


import java.util.Map;
import net.sf.freecol.common.option.AbstractOption;
import java.util.HashMap;
import net.sf.freecol.common.option.IntegerOption;
import net.sf.freecol.common.option.RangeOption;
import net.sf.freecol.common.option.BooleanOption;
import net.sf.freecol.common.option.StringOption;

public class SpecificationProduct {private final Map<String, AbstractOption> allOptions=new HashMap<>();public Map<String, AbstractOption> getAllOptions(){return allOptions;}/**
 * Is option with this identifier present?  This is helpful when options are optionally(!) present, for example model.option.priceIncrease.artillery exists but model.option.priceIncrease.frigate does not.
 * @param id  The object identifier to test.
 * @return  True/false on presence of option id.
 */public boolean hasOption(String id){return id != null && allOptions.containsKey(id);}/**
 * Get the <code>AbstractOption</code> with the given identifier.
 * @param id  The object identifier.
 * @return  The <code>AbstractOption</code> found.
 * @exception IllegalArgumentException  if the identifier is null or not present.
 */public AbstractOption getOption(String id) throws IllegalArgumentException{if (id == null){throw new IllegalArgumentException("AbstractOption with null id.");} else if (!allOptions.containsKey(id)){throw new IllegalArgumentException("Missing AbstractOption: " + id);} else {return allOptions.get(id);}}/**
 * Adds an <code>AbstractOption</code> to this specification.
 * @param abstractOption  The <code>AbstractOption</code> to add.
 */public void addAbstractOption(AbstractOption abstractOption){allOptions.put(abstractOption.getId(),abstractOption);}/**
 * Get the <code>IntegerOption</code> with the given identifier.
 * @param id  The object identifier.
 * @return  The <code>IntegerOption</code> found.
 */public IntegerOption getIntegerOption(String id){return (IntegerOption)getOption(id);}/**
 * Get the <code>RangeOption</code> with the given identifier.
 * @param id  The object identifier.
 * @return  The <code>RangeOption</code> found.
 */public RangeOption getRangeOption(String id){return (RangeOption)getOption(id);}/**
 * Get the <code>BooleanOption</code> with the given identifier.
 * @param id  The object identifier.
 * @return  The <code>BooleanOption</code> found.
 */public BooleanOption getBooleanOption(String id){return (BooleanOption)getOption(id);}/**
 * Get the <code>StringOption</code> with the given identifier.
 * @param id  The object identifier.
 * @return  The <code>StringOption</code> found.
 */public StringOption getStringOption(String id){return (StringOption)getOption(id);}/**
 * Gets the boolean value of an option.
 * @param id  The object identifier.
 * @return  The value.
 * @exception IllegalArgumentException  If there is no boolean value associated with the specified option.
 * @exception NullPointerException  if the given <code>Option</code> does not exist.
 */public boolean getBoolean(String id){try {return getBooleanOption(id).getValue();} catch (ClassCastException e){throw new IllegalArgumentException("Not a boolean option: " + id,e);}}/**
 * Gets the integer value of an option.
 * @param id  The object identifier.
 * @return  The value.
 * @exception IllegalArgumentException  If there is no integer value associated with the specified option.
 * @exception NullPointerException  if the given <code>Option</code> does not exist.
 */public int getInteger(String id){try {return getIntegerOption(id).getValue();} catch (ClassCastException e){throw new IllegalArgumentException("Not an integer option: " + id,e);}}/**
 * Gets the string value of an option.
 * @param id  The object identifier.
 * @return  The value.
 * @exception IllegalArgumentException  If there is no string value associated with the specified option.
 * @exception NullPointerException  if the given <code>Option</code> does not exist.
 */public String getString(String id){try {return getStringOption(id).getValue();} catch (ClassCastException e){throw new IllegalArgumentException("Not a string option: " + id,e);}}}