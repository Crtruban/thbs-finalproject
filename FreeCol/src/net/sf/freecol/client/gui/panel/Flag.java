/**
 *  Copyright (C) 2002-2015   The FreeCol Team
 *
 *  This file is part of FreeCol.
 *
 *  FreeCol is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  FreeCol is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FreeCol.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.sf.freecol.client.gui.panel;

import java.awt.Color;
import java.awt.Graphics2D; // refactor #149 Checkstyle lexicographical order
import java.awt.RenderingHints; // refactor #150 Checkstyle lexicographical order
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

// TODO: Auto-generated Javadoc
/**
 * This class generates the most common types of flags from a small number of parameters, biased
 * towards flags similar to that of the United States, i.e. flags with a "union", an area filled
 * with stars representing the colonies of the player. Obvious improvements include adding shapes
 * other than stars (e.g. the fleur-de-lys for Quebec) and larger design elements, such as the
 * Southern Cross, or coats of arms.
 *
 * <p>Please feel free to correct any vexillological errors.
 */
public class Flag {

  /** The flag color. */
  private FlagColor flagColor = new FlagColor();
  // refactor #27 -- Extract god class bad smell (flagcolor.java)

  /**
   * The alignment of stripes or other design elements on the flag. Might be extended to handle
   * diagonal alignment (Friesland, for example).
   */
  public enum Alignment {
    
    /** The none. */
    NONE, 
 /** The horizontal. */
 HORIZONTAL, 
 /** The vertical. */
 VERTICAL
  } // refactor #151 Checkstyle } should be alone on line

  /**
   * The "background layer" of the flag, generally one or several squares or triangles of different
   * color. The alignment of the background influences the size of the canton, if there is one.
   */
  public enum Background {
    /** A plain background. */
    PLAIN(Alignment.NONE),
    /** Quartered rectangularly. */
    QUARTERLY(Alignment.NONE),
    /** Vertical stripes. */
    PALES(Alignment.VERTICAL),
    /** Horizontal stripes. */
    FESSES(Alignment.HORIZONTAL),
    /** Diagonal top left to bottom right. */
    PER_BEND(Alignment.NONE),
    /** Diagonal bottom left to top right. */
    PER_BEND_SINISTER(Alignment.NONE),
    /** Quartered diagonally. */
    PER_SALTIRE(Alignment.NONE);

    /** The alignment. */
    public final Alignment alignment;

    /**
     * Instantiates a new background.
     *
     * @param alignment the alignment
     */
    Background(Alignment alignment) {
      this.alignment = alignment;
    }
  } // refactor #152 Checkstyle } should be alone on line

  /**
   * The "middle layer" of the flag, generally a number of vertical, horizontal or diagonal bars.
   * The decoration limits the shape and possible positions of the "union".
   */
  public enum Decoration {
    
    /** The none. */
    NONE(), 
 /** The cross. */
 CROSS(UnionPosition.CANTON), 
 /** The greek cross. */
 GREEK_CROSS(UnionPosition.CANTON),
    
    /** The scandinavian cross. */
    SCANDINAVIAN_CROSS(UnionPosition.CANTON), 
 /** The chevron. */
 CHEVRON(UnionShape.CHEVRON, UnionPosition.LEFT),
    
    /** The pall. */
    PALL(UnionShape.CHEVRON, UnionPosition.LEFT),
    
    /** The bend. */
    BEND(UnionShape.BEND, UnionPosition.TOP, UnionPosition.BOTTOM),
    
    /** The bend sinister. */
    BEND_SINISTER(UnionShape.BEND, UnionPosition.TOP, UnionPosition.BOTTOM),
    
    /** The saltire. */
    SALTIRE(UnionShape.CHEVRON, UnionPosition.TOP, UnionPosition.BOTTOM, UnionPosition.LEFT,
        UnionPosition.RIGHT),
    
    /** The saltire and cross. */
    SALTIRE_AND_CROSS(UnionPosition.CANTON);

    /** The union shape. */
    public UnionShape unionShape = UnionShape.RECTANGLE;
    
    /** The union positions. */
    public Set<UnionPosition> unionPositions = EnumSet.allOf(UnionPosition.class);

    /**
     * Instantiates a new decoration.
     *
     * @param positions the positions
     */
    Decoration(UnionPosition... positions) {
      this.unionPositions = EnumSet.of(UnionPosition.NONE);
      for (UnionPosition position : positions) {
        unionPositions.add(position);
      }
    }

    /**
     * Instantiates a new decoration.
     *
     * @param shape the shape
     * @param positions the positions
     */
    Decoration(UnionShape shape, UnionPosition... positions) {
      this(positions);
      this.unionShape = shape;
    }

  } // refactor #153 Checkstyle } should be alone on line

  /**
   * The shape of the "union", which generally depends on the decoration or background of the flag.
   */
  public enum UnionShape {
    
    /** The rectangle. */
    RECTANGLE, 
 /** The triangle. */
 TRIANGLE, 
 /** The chevron. */
 CHEVRON, 
 /** The bend. */
 BEND, 
 /** The rhombus. */
 RHOMBUS
  } // refactor #154 Checkstyle } should be alone on line

  /**
   * The position of the "union", which depends on the alignment of the background. The "canton" is
   * the top left quarter of the flag. Other quarters might be added.
   */
  public enum UnionPosition {
    
    /** The left. */
    LEFT(Alignment.VERTICAL, 0), 
 /** The center. */
 CENTER(Alignment.VERTICAL, 1), 
 /** The right. */
 RIGHT(Alignment.VERTICAL, 2),
    
    /** The top. */
    TOP(Alignment.HORIZONTAL, 0), 
 /** The middle. */
 MIDDLE(Alignment.HORIZONTAL, 1), 
 /** The bottom. */
 BOTTOM(Alignment.HORIZONTAL, 2),
    
    /** The canton. */
    CANTON(Alignment.NONE, 0), 
 /** The none. */
 NONE(null, 0);

    /** The alignment. */
    public final Alignment alignment;
    
    /** The index. */
    public final int index;

    /**
     * Instantiates a new union position.
     *
     * @param alignment the alignment
     * @param index the index
     */
    UnionPosition(Alignment alignment, int index) {
      this.alignment = alignment;
      this.index = index;
    }

  }

  /**
   * Width and height are used in the usual sense, not the vexillological sense.
   */
  public static final int WIDTH = 150;
  
  /** The Constant WIDTH_HALF. */
  public static final int WIDTH_HALF = WIDTH / 2;
  
  /** The Constant HEIGHT. */
  // refactor #39 codepro audit added new constant
  public static final int HEIGHT = 100;
  
  /** The Constant HEIGHT_HALF. */
  public static final int HEIGHT_HALF = HEIGHT / 2;
  
  /** The Constant SQRT_3. */
  // refactor #40 codepro audit added new constant
  public static final double SQRT_3 = Math.sqrt(3);

  /**
   * MAGIC NUMBER: the width of decoration elements.
   */
  public static final double DECORATION_SIZE = (double) HEIGHT / 7;
  
  /** The Constant DECORATION_SIZE_DOUBLED. */
  public static final double DECORATION_SIZE_DOUBLED = DECORATION_SIZE * 2;
  
  /** The Constant CHEVRON_X. */
  // refactor #50 codepro audit added new constant
  public static final double CHEVRON_X = SQRT_3 * HEIGHT / 2;
  /**
   * MAGIC NUMBER: the size of the stars in the union.
   */
  public static final double STAR_SIZE = 0.07 * HEIGHT;
  /**
   * MAGIC NUMBER: the horizontal offset of the vertical bar of the Scandinavian cross.
   */
  public static final double CROSS_OFFSET = 2 * DECORATION_SIZE;
  
  /** The Constant BEND_X. */
  public static final double BEND_X = DECORATION_SIZE;
  
  /** The Constant BEND_Y. */
  public static final double BEND_Y = DECORATION_SIZE / SQRT_3;
  
  /** The Constant BEND_X_DOUBLED. */
  public static final double BEND_X_DOUBLED = BEND_X * 2;
  
  /** The Constant BEND_Y_DOUBLED. */
  // refactor #45 added new constant for expression val of constant
  public static final double BEND_Y_DOUBLED = BEND_Y * 2;
  // refactor #46 added new constant for expression val of constant

  /** The Constant star. */
  private static final GeneralPath star = getStar();

  /**
   * The distribution of stars in the "union", based on historical flags of the United States. This
   * really should be extended to handle rectangles of different shapes.
   */
  private static final int[][] layout = new int[51][2];

  static {
    for (int[] bars : new int[][] { { 5, 4 }, { 5, 6 }, { 6, 5 }, { 5, 5 } }) {
      int sum = bars[0] + bars[1];
      boolean even = true;
      while (sum < 51) {
        layout[sum] = bars;
        if (even) {
          sum += bars[0];
          even = false;
        } else {
          sum += bars[1];
          even = true;
        }
      }
    }

  }

  /**
   * If the background is striped, the background colors will be used in turn, from top to bottom,
   * or from left to right. If the background is quartered, the colors will be used clockwise,
   * starting at the top left. If there are more colors than stripes or quarters, the superfluous
   * colors will be ignored.
   */
  private List<Color> backgroundColors = new ArrayList<>();

  /** The background. */
  private Background background = Background.FESSES;
  
  /** The decoration. */
  private Decoration decoration = Decoration.NONE;
  
  /** The union shape. */
  private UnionShape unionShape = UnionShape.RECTANGLE;
  
  /** The union position. */
  private UnionPosition unionPosition = UnionPosition.CANTON;

  /**
   * The number of stars in the union. This value is only relevant if the flag contains a union.
   */
  private int stars = 13;

  /**
   * The number of background stripes. This value is only relevant if the background contains
   * stripes (currently FESSES or PALES).
   */
  private int stripes = 13;

  /**
   * Instantiates a new flag.
   *
   * @param background the background
   * @param decoration the decoration
   * @param unionPosition the union position
   */
  public Flag(Background background, Decoration decoration, UnionPosition unionPosition) {
    this(background, decoration, unionPosition, UnionShape.RECTANGLE);
  }

  /**
   * Instantiates a new flag.
   *
   * @param background the background
   * @param decoration the decoration
   * @param unionPosition the union position
   * @param unionShape the union shape
   */
  public Flag(Background background, Decoration decoration, UnionPosition unionPosition,
      UnionShape unionShape) {
    this.background = background;
    this.decoration = decoration;
    this.unionPosition = unionPosition;
    this.unionShape = unionShape;
  }

  /**
   * Returns the background of the flag.
   *
   * @return A <code>Background</code> value.
   */
  public Background getBackground() {
    return background;
  }

  /**
   * Sets the background of the flag and returns the flag itself.
   *
   * @param background The new <code>Background</code> value.
   * @return The modified flag.
   */
  public Flag setBackground(Background background) {
    this.background = background;
    return this;
  }

  /**
   * Returns the decoration of the flag.
   *
   * @return A <code>Decoration</code> value.
   */
  public Decoration getDecoration() {
    return decoration;
  }

  /**
   * Sets the decoration of the flag and returns the flag itself.
   *
   * @param decoration The new <code>Decoration</code> value.
   * @return The modified flag.
   */
  public Flag setDecoration(Decoration decoration) {
    this.decoration = decoration;
    return this;
  }

  /**
   * Returns the union position of the flag.
   *
   * @return A <code>UnionPosition</code> value.
   */
  public UnionPosition getUnionPosition() {
    return unionPosition;
  }

  /**
   * Sets the union position of the flag and returns the flag itself.
   *
   * @param position The new <code>UnionPosition</code> value.
   * @return The modified flag.
   */
  public Flag setUnionPosition(UnionPosition position) {
    this.unionPosition = position;
    return this;
  }

  /**
   * Returns the union shape of the flag.
   *
   * @return A <code>UnionShape</code> value.
   */
  public UnionShape getUnionShape() {
    return unionShape;
  }

  /**
   * Sets the union shape of the flag and returns the flag itself.
   *
   * @param shape The new <code>UnionShape</code> value.
   * @return The modified flag.
   */
  public Flag setUnionShape(UnionShape shape) {
    this.unionShape = shape;
    return this;
  }

  /**
   * Returns a <code>List</code> of background colors.
   *
   * @return A <code>List</code> of background colors.
   */
  public List<Color> getBackgroundColors() {
    return backgroundColors;
  }

  /**
   * Sets the background colors of the flag and returns the flag itself.
   *
   * @param backgroundColors A <code>List</code> of background colors.
   * @return The modified flag.
   */
  public Flag setBackgroundColors(List<Color> backgroundColors) {
    this.backgroundColors = backgroundColors;
    return this;
  }

  /**
   * Sets the background colors of the flag and returns the flag itself.
   *
   * @param colors A variable number of background colors.
   * @return The modified flag.
   */
  public Flag setBackgroundColors(Color... colors) {
    backgroundColors.clear();
    for (Color color : colors) {
      if (color != null) {
        backgroundColors.add(color);
      }
    }
    return this;
  }

  /**
   * Returns the union color of the flag.
   *
   * @return A <code>Color</code> value.
   */
  public Color getUnionColor() {
    return flagColor.getUnionColor();
  }

  /**
   * Sets the union color of the flag and returns the flag itself.
   *
   * @param unionColor The new <code>Color</code> value.
   * @return The modified flag.
   */
  public Flag setUnionColor(Color unionColor) {
    return flagColor.setUnionColor(unionColor, this);
  }

  /**
   * Returns the decoration color of the flag.
   *
   * @return A <code>Color</code> value.
   */
  public Color getDecorationColor() {
    return flagColor.getDecorationColor();
  }

  /**
   * Sets the decoration color of the flag and returns the flag itself.
   *
   * @param decorationColor The new <code>Color</code> value.
   * @return The modified flag.
   */
  public Flag setDecorationColor(Color decorationColor) {
    return flagColor.setDecorationColor(decorationColor, this);
  }

  /**
   * Returns the star color of the flag.
   *
   * @return A <code>Color</code> value.
   */
  public Color getStarColor() {
    return flagColor.getStarColor();
  }

  /**
   * Sets the star color of the flag and returns the flag itself.
   *
   * @param starColor The new <code>Color</code> value.
   * @return The modified flag.
   */
  public Flag setStarColor(Color starColor) {
    return flagColor.setStarColor(starColor, this);
  }

  /**
   * Returns the number of stars in the union.
   *
   * @return The number of stars.
   */
  public int getStars() {
    return stars;
  }

  /**
   * Sets the number of stars in the union and returns the flag itself.
   *
   * @param stars The new number of stars.
   * @return The modified flag.
   */
  public Flag setStars(int stars) {
    this.stars = stars;
    return this;
  }

  /**
   * Returns the number of background stripes.
   *
   * @return The number of stars.
   */
  public int getStripes() {
    return stripes;
  }

  /**
   * Sets the number of background stripes and returns the flag itself.
   *
   * @param stripes The new number of background stripes.
   * @return The modified flag.
   */
  public Flag setStripes(int stripes) {
    this.stripes = stripes;
    return this;
  }

  /**
   * Generates an image of the flag.
   *
   * @return An image of the flag.
   */
  public BufferedImage getImage() {
    final BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
    // refactor #65 variable should be final
    final Graphics2D graphic = image.createGraphics();
    // refactor #28 CodePro audit questionable name, refactor #66 variable should be final
    graphic.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

    if (backgroundColors.isEmpty()) {
      backgroundColors.add(Color.BLACK);
    }
    // draw background
    switch (background) {
      case FESSES:
      case PALES:
        if (stripes < 1) {
          drawBackground(graphic);
        } else {
          drawStripes(graphic, background.alignment, stripes);
        }
        break;
      case PLAIN:
        drawBackground(graphic);
        break;
      case QUARTERLY:
        drawQuarters(graphic);
        break;
      case PER_BEND:
        drawPerBend(graphic, false);
        break;
      case PER_BEND_SINISTER:
        drawPerBend(graphic, true);
        break;
      case PER_SALTIRE:
        drawPerSaltire(graphic);
        break;
      default:
        break;
    }

    // draw decoration
    final GeneralPath decorationShape = getImage_DecorationShape();
    // refactor #24 extracted long method bad small, refactor #67 variable should be final
    if (decorationShape != null) {
      graphic.setColor(flagColor.getDecorationColor());
      graphic.fill(decorationShape);
    }

    if (unionPosition == null || unionPosition == UnionPosition.NONE) {
      return image;
    }

    GeneralPath union = null;
    final GeneralPath starShape = getImage_drawStarShape();
    // refactor #25 extracted long method bad small, refactor #68 variable should be final
    switch (unionShape) {
      case RECTANGLE:
        final Rectangle2D.Double rectangle = getRectangle();
        // refactor #69 variable should be final
        union = new GeneralPath(rectangle);
        break;
      case CHEVRON:
        union = getTriangle(unionShape, decoration == Decoration.PALL);
        break;
      case BEND:
        union = getTriangle(unionShape,
            (decoration == Decoration.BEND) || (decoration == Decoration.BEND_SINISTER));
        transformBend(union);
        transformBend(starShape);
        break;
      case RHOMBUS:
        union = getRhombus();
        break;
      case TRIANGLE:
        union = getTriangle(unionShape, decoration == Decoration.SALTIRE);
        transformTriangle(union);
        transformTriangle(starShape);
        break;
      default:
        break;
    }
    if (!(union == null || flagColor.getUnionColor() == null)) {
      graphic.setColor(flagColor.getUnionColor());
      graphic.fill(union);
    }
    if (starShape != null) {
      graphic.setColor(flagColor.getStarColor());
      graphic.fill(starShape);
    }
    graphic.dispose();
    return image;
  }

  /**
   * Gets the image draw star shape.
   *
   * @return the image draw star shape
   */
  private GeneralPath getImage_drawStarShape() {
    GeneralPath starShape = null;
    if (unionShape == null && decoration != null) {
      unionShape = decoration.unionShape;
    }
    starShape = getImage_starShape(starShape);
    // refactor #26 extracted long method bad small
    return starShape;
  }

  /**
   * Gets the image star shape.
   *
   * @param starShape the star shape
   * @return the image star shape
   */
  private GeneralPath getImage_starShape(GeneralPath starShape) {
    switch (unionShape) {
      case RECTANGLE:
        final Rectangle2D.Double rectangle = getRectangle();
        // refactor #70 variable should be final
        starShape = getUnionRectangle(rectangle);
        break;
      case CHEVRON:
        starShape = getUnionTriangle(true);
        break;
      case BEND:
        starShape = getUnionTriangle(false);
        break;
      case RHOMBUS:
        starShape = getUnionRhombus();
        break;
      case TRIANGLE:
        starShape = getUnionTriangle(true);
        break;

    }
    return starShape;
  }

  /**
   * Gets the image decoration shape.
   *
   * @return the image decoration shape
   */
  public GeneralPath getImage_DecorationShape() {
    GeneralPath decorationShape = null;
    switch (decoration) {
      case GREEK_CROSS:
      case CROSS:
      case SCANDINAVIAN_CROSS:
        decorationShape = getCross(decoration);
        break;
      case CHEVRON:
        decorationShape = getTriangle(UnionShape.CHEVRON, false);
        break;
      case PALL:
        decorationShape = getPall();
        break;
      case BEND:
        decorationShape = getBend(false);
        break;
      case BEND_SINISTER:
        decorationShape = getBend(true);
        break;
      case SALTIRE:
        decorationShape = getBend(true);
        decorationShape.append(getBend(false), false);
        break;
      case SALTIRE_AND_CROSS:
        decorationShape = getBend(true);
        decorationShape.append(getBend(false), false);
        decorationShape.append(getCross(Decoration.CROSS), false);
        break;
      default:
        break;
    }
    return decorationShape;
  }

  /**
   * Return the stars in a rectangular "union", distributed according to rules derived from
   * historical flags of the United States.
   *
   * @param union The rectangular area to fill.
   * @return The union path
   */
  private GeneralPath getUnionRectangle(Rectangle2D.Double union) {

    if (union == null) { // refactor #155 Checkstyle 'if' must use {}'s
      return null;
    }

    GeneralPath unionPath;
    if (stars < 14) {
      final double radius = 0.3 * Math.min(union.height, union.width);
      // refactor #71 variable should be final
      unionPath = getCircleOfStars(radius);
    } else {
      unionPath = getGridOfStars(union);
    }
    final double x = union.x + union.width / 2;
    // refactor #72 variable should be final
    final double y = union.y + union.height / 2;
    // refactor #73 variable should be final
    center(unionPath, x, y);
    return unionPath;
  }

  /**
   * Gets the union triangle.
   *
   * @param isosceles the isosceles
   * @return the union triangle
   */
  private GeneralPath getUnionTriangle(boolean isosceles) {
    final boolean small = getUnionTriangle_IsSmall();
    // refactor #8 extracted long method bad smell, refactor #74 variable should be final

    final double x = getUnionTriangle_xByShape(small);
    // refactor #9 extracted long method bad small, refactor #75 variable should be final
    final double y = getUnionTriangle_yByShape(small);
    // refactor #10 extracted long method bad small, refactor #76 variable should be final
    final double radius = getUnionTriangle_rByShape(small, x, y);
    // refactor #11 extracted long method bad small, refactor #77 variable should be final
    // refactor #30 CodePro audit questionable name ('r' to 'radius')

    // leave a margin
    final double radiusWithMargin = radius * 0.6;
    // refactor #29 CodePro audit questionable name ('radius' to 'radiusWithMargin'),
    // refactor #78 variable should be final

    GeneralPath unionPath = new GeneralPath();
    if (stars < 14) {
      unionPath = getCircleOfStars(radiusWithMargin);
      if (isosceles) {
        if (unionPosition == UnionPosition.LEFT || unionPosition == UnionPosition.RIGHT) {
          center(unionPath, radius, HEIGHT_HALF);
          // refactor #41 CodePro audit expression evaluation (expression value is constant)
        } else {
          center(unionPath, radius, WIDTH_HALF);
          // refactor #42 CodePro audit expression evaluation (expression value is constant)
        }
      } else {
        center(unionPath, radius, radius);
      }
    } else {

      int sum = 0;
      int rows = 1;
      while (sum < stars) {
        sum += rows;
        rows++;
      }
      int missing = sum - stars;
      final double slope = y / x; // refactor #79 variable should be final
      final double dx = x / rows; // refactor #80 variable should be final
      double xx = dx / 2;
      double height = y - xx * slope;
      double offset = 0;
      rows--;
      double dy = height / rows;
      for (int index = rows; index > 0; index--) {
        dy = getUnionTriangle_dy(isosceles, y, slope, xx, height, dy, index);
        height = getUnionTriangle_Height(isosceles, y, slope, xx, height);
        if (isosceles) {
          offset = (HEIGHT - height) / 2;
        }

        double yy = 0;
        yy = (missing > 0) ? (yy + (missing / 2 * dy)) : (dy / 2);
        // refactor #20 combined lines into Ternary Operator

        int count = (missing > 0) ? (index - missing) : index;
        // refactor #21 combined lines into Ternary Operator
        if (missing > 0) {
          // count = index - missing;
          yy += missing / 2 * dy;
          missing = 0;
        }
        for (int star = 0; star < count; star++) {
          unionPath.append(getStar(xx, yy + offset), false);
          yy += dy;
        }
        xx += dx;
      }
    }
    return unionPath;
  }

  /**
   * Gets the union triangle dy.
   *
   * @param isosceles the isosceles
   * @param y the y
   * @param slope the slope
   * @param xx the xx
   * @param height the height
   * @param dy the dy
   * @param index the index
   * @return the union triangle dy
   */
  private double getUnionTriangle_dy(boolean isosceles, double y, double slope, double xx,
      double height, double dy, int index) {
    height = getUnionTriangle_Height(isosceles, y, slope, xx, height);
    if (isosceles) {
      dy = height / index;
    }
    return dy;
  }

  /**
   * Gets the union triangle height.
   *
   * @param isosceles the isosceles
   * @param y the y
   * @param slope the slope
   * @param xx the xx
   * @param height the height
   * @return the union triangle height
   */
  private double getUnionTriangle_Height(boolean isosceles, double y, double slope, double xx,
      double height) {
    if (isosceles) {
      height = y - xx * slope;
    }
    return height;
  }

  /**
   * Gets the union triangle is small.
   *
   * @return the union triangle is small
   */
  public boolean getUnionTriangle_IsSmall() {
    final boolean small = (decoration == Decoration.PALL || decoration == Decoration.BEND
        || decoration == Decoration.BEND_SINISTER);
    // refactor #81 variable should be final
    return small;
  }

  /**
   * Gets the union triangle x by shape.
   *
   * @param small the small
   * @return the union triangle x by shape
   */
  private double getUnionTriangle_xByShape(boolean small) {
    // refactor #158 Checkstyle no consecutive capitals
    double x = 0;
    if (unionShape == UnionShape.CHEVRON) {
      x = (small) ? (x - BEND_X) : CHEVRON_X;
      // refactor #12 combined lines into Ternary Operator
    } else if (unionShape == UnionShape.TRIANGLE) {
      if (unionPosition == UnionPosition.LEFT || unionPosition == UnionPosition.RIGHT) {
        x = (small) ? (x - BEND_X) : (WIDTH_HALF);
        // refactor #13 combined lines into Ternary Operator, refactor #43 CodePro audit expression
        // evaluation
      } else {
        x = (small) ? (x - BEND_Y) : (HEIGHT_HALF);
        // refactor #14 combined lines into Ternary Operator, refactor #44 CodePro audit expression
        // evaluation
      }
    } else {
      // union shape is bend
      x = (small) ? (x - BEND_X) : WIDTH;
      // refactor #15 combined lines into Ternary Operator
    }
    return x;
  }

  /**
   * Gets the union triangle y by shape.
   *
   * @param small the small
   * @return the union triangle y by shape
   */
  private double getUnionTriangle_yByShape(boolean small) {
    // refactor #159 Checkstyle no consecutive capitals
    double y = 0;

    if (unionShape == UnionShape.CHEVRON) {
      y = (small) ? (y - (2 * BEND_Y)) : HEIGHT;
      // refactor #16 combined lines into Ternary` Operator
    } else if (unionShape == UnionShape.TRIANGLE) {
      if (unionPosition == UnionPosition.LEFT || unionPosition == UnionPosition.RIGHT) {
        y = (small) ? (y - (BEND_Y_DOUBLED)) : HEIGHT;
        // refactor #17 combined lines into Ternary Operator, refactor #47 CodePro audit expression
        // evaluation
      } else {
        y = (small) ? (y - (BEND_X_DOUBLED)) : WIDTH;
        // refactor #18 combined lines into Ternary Operator, refactor #48 CodePro audit expression
        // evaluation
      }
    } else {
      y = (small) ? (y - BEND_Y) : HEIGHT;
      // refactor #19 combined lines into Ternary Operator
    }

    return y;
  }

  /**
   * Gets the union triangle r by shape.
   *
   * @param small the small
   * @param x the x
   * @param y the y
   * @return the union triangle r by shape
   */
  private double getUnionTriangle_rByShape(boolean small, double x, double y) {
    // refactor #160 Checkstyle no consecutive capitals
    double radius = 0;
    // refactor #31 CodePro audit questionable name ('r' to 'radius')

    if (unionShape == UnionShape.CHEVRON) {
      radius = SQRT_3 * y / 6;
    } else if (unionShape == UnionShape.TRIANGLE) {
      if (unionPosition == UnionPosition.LEFT || unionPosition == UnionPosition.RIGHT) {
        radius = SQRT_3 * y / 6;
      } else {
        final double height = y / 2;
        // refactor #32 CodePro audit questionable name ('h' to 'height'),
        // refactor #82 variable should be final
        final double longSide = Math.sqrt(height * height + x * x);
        // refactor #33 CodePro audit questionable name ('c' to 'longSide'),
        // refactor #83 variable should be final
        radius = x * y / (y + 2 * longSide);
      }
    } else {
      // union shape is bend
      final double longSide = Math.sqrt(x * x + y * y);
      // refactor #34 CodePro audit questionable name ('c' to 'longSide'),
      // refactor #85 variable should be final
      final double area = x * y / 2;
      // refactor #35 CodePro audit questionable name ('A' to 'area'),
      // refactor #84 variable should be final
      radius = 2 * area / (x + y + longSide);
    }

    return radius;
  }

  /**
   * Flip or rotate a top left triangle so that it fits another corner.
   *
   * @param triangle The top left triangle.
   * @return The transformed triangle.
   */
  private GeneralPath transformBend(GeneralPath triangle) {
    if (unionPosition == UnionPosition.TOP) {
      if (decoration == Decoration.BEND) {
        triangle.transform(AffineTransform.getScaleInstance(-1, 1));
        triangle.transform(AffineTransform.getTranslateInstance(WIDTH, 0));
      } else if (decoration == Decoration.BEND_SINISTER) {
        // nothing to do: default is top left
      }
    } else if (unionPosition == UnionPosition.BOTTOM) {
      if (decoration == Decoration.BEND) {
        triangle.transform(AffineTransform.getScaleInstance(1, -1));
        triangle.transform(AffineTransform.getTranslateInstance(0, HEIGHT));
      } else if (decoration == Decoration.BEND_SINISTER) {
        triangle.transform(AffineTransform.getQuadrantRotateInstance(2));
        triangle.transform(AffineTransform.getTranslateInstance(WIDTH, HEIGHT));
      }
    }
    return triangle;
  }

  /**
   * Flip or rotate a left triangle so that it fits another side.
   *
   * @param triangle The left triangle.
   * @return The transformed triangle.
   */
  private GeneralPath transformTriangle(GeneralPath triangle) {
    switch (unionPosition) {
      case TOP:
        triangle.transform(AffineTransform.getQuadrantRotateInstance(1));
        triangle.transform(AffineTransform.getTranslateInstance(WIDTH, 0));
        break;
      case BOTTOM:
        triangle.transform(AffineTransform.getQuadrantRotateInstance(3));
        triangle.transform(AffineTransform.getTranslateInstance(0, HEIGHT));
        break;
      case RIGHT:
        triangle.transform(AffineTransform.getScaleInstance(-1, 1));
        triangle.transform(AffineTransform.getTranslateInstance(WIDTH, 0));
        break;
      case LEFT:
      default:
    }
    return triangle;
  }

  /**
   * Gets the union rhombus.
   *
   * @return the union rhombus
   */
  private GeneralPath getUnionRhombus() {
    GeneralPath unionPath = new GeneralPath();

    // refactor #1 -- bad smells long method extraction
    final int count = unionRhombus_CountStars();
    // refactor #86 variable should be final
    final int rows = unionRhombus_CountRows(count);
    // refactor #87 variable should be final
    int starCount = 0;
    final double side1 = WIDTH / 2 - BEND_X;
    // refactor #36 CodePro audit questionable name ('a' to 'side1'),
    // refactor #88 variable should be final
    final double b = HEIGHT / 2 - BEND_Y;
    // refactor #89 variable should be final

    if (stars < 14) {
      final double c = Math.sqrt(side1 * side1 + b * b);
      // refactor #90 variable should be final
      final double radius = (side1 * b) / c;
      // refactor #38 CodePro audit questionable name ('r' to 'radius'),
      // refactor #91 variable should be final
      final double radiusWithMargin = 0.6 * radius;
      // refactor #37 CodePro audit questionable name ('radius' to 'radiusWithMargin'),
      // refactor #92 variable should be final
      unionPath = getCircleOfStars(radiusWithMargin);
      center(unionPath, WIDTH_HALF, HEIGHT_HALF);
      // refactor #49 CodePro audit expression evaluation WIDTH_HALF and HEIGHT_HALF
    } else {
      final double dx = side1 / count; // refactor #93 variable should be final
      final double dy = b / count; // refactor #94 variable should be final
      final double dx1 = side1 / rows; // refactor #95 variable should be final
      final double dy1 = b / rows; // refactor #96 variable should be final
      outer: for (int index = 0; index < rows; index++) {
        double x = BEND_X + dx + index * dx1;
        double y = HEIGHT / 2 - index * dy1;
        for (int star = 0; star < count; star++) {
          unionPath.append(getStar(x, y), false);
          starCount++;
          if (starCount == stars) {
            break outer;
          } else {
            x += dx;
            y += dy;
          }
        }
      }
    }

    return unionPath;
  }

  /**
   * Union rhombus count rows.
   *
   * @param count the count
   * @return the int
   */
  public int unionRhombus_CountRows(int count) {
    int rows = stars / count;
    if (rows * count < stars) { // refactor #156 Checkstyle 'if' must use {}'s
      rows++;
    }
    return rows;
  }

  /**
   * Union rhombus count stars.
   *
   * @return the int
   */
  public int unionRhombus_CountStars() {
    int count = 1;
    int square = 1;
    while (square < stars) {
      square = unionRhombus_CountSquares(count, square);
      count++;
    }
    return count;
  }

  /**
   * Union rhombus count squares.
   *
   * @param count the count
   * @param square the square
   * @return the int
   */
  private int unionRhombus_CountSquares(int count, int square) {
    count++;
    square = count * count;
    return square;
  }

  /**
   * Calculate the width of stripes on the basis of the given alignment.
   *
   * @param alignment The alignment of the stripes.
   * @return The width of the stripes.
   */
  private double getStripeWidth(Alignment alignment) {
    return (alignment == Alignment.HORIZONTAL) ? WIDTH : (double) WIDTH / stripes;
  }

  /**
   * Calculate the height of stripes on the basis of the given alignment.
   *
   * @param alignment The alignment of the stripes.
   * @return The height of the stripes.
   */
  private double getStripeHeight(Alignment alignment) {
    return (alignment == Alignment.VERTICAL) ? HEIGHT : (double) HEIGHT / stripes;
  }

  /**
   * Draw background.
   *
   * @param g the g
   */
  private void drawBackground(Graphics2D g) {
    g.setColor(backgroundColors.get(0));
    g.fillRect(0, 0, WIDTH, HEIGHT);
  }

  /**
   * Draw stripes.
   *
   * @param g the g
   * @param alignment the alignment
   * @param stripes the stripes
   */
  private void drawStripes(Graphics2D g, Alignment alignment, int stripes) {
    final int colors = backgroundColors.size();
    // refactor #97 variable should be final
    final double stripeWidth = getStripeWidth(alignment);
    // refactor #98 variable should be final
    final double stripeHeight = getStripeHeight(alignment);
    // refactor #99 variable should be final
    final double x = (alignment == Alignment.VERTICAL) ? stripeWidth : 0;
    // refactor #100 variable should be final
    final double y = (alignment == Alignment.HORIZONTAL) ? stripeHeight : 0;
    // refactor #101 variable should be final
    final Rectangle2D.Double rectangle = new Rectangle2D.Double();
    // refactor #102 variable should be final
    drawStripes_Loop(g, stripes, colors, stripeWidth, stripeHeight, x, y, rectangle);
    // refactor #6 bad smells long method extraction
  }

  /**
   * Draw stripes loop.
   *
   * @param g the g
   * @param stripes the stripes
   * @param colors the colors
   * @param stripeWidth the stripe width
   * @param stripeHeight the stripe height
   * @param x the x
   * @param y the y
   * @param rectangle the rectangle
   */
  public void drawStripes_Loop(Graphics2D g, int stripes, int colors, double stripeWidth,
      double stripeHeight, double x, double y, Rectangle2D.Double rectangle) {
    for (int index = 0; index < stripes; index++) {
      g.setColor(backgroundColors.get(index % colors));
      rectangle.setRect(index * x, index * y, stripeWidth, stripeHeight);
      g.fill(rectangle);
    }
  }

  /**
   * Draw quarters.
   *
   * @param g the g
   */
  private void drawQuarters(Graphics2D g) {
    final int colors = backgroundColors.size(); // refactor #105 variable should be final
    final int[] x = { 0, 1, 1, 0 }; // refactor #106 variable should be final
    final int[] y = { 0, 0, 1, 1 }; // refactor #107 variable should be final
    final double halfWidth = WIDTH / 2; // refactor #108 variable should be final
    final double halfHeight = HEIGHT / 2; // refactor #103 variable should be final
    final double offset = (decoration == Decoration.SCANDINAVIAN_CROSS) ? CROSS_OFFSET : 0;
    // refactor #104 variable should be final
    final Rectangle2D.Double rectangle = new Rectangle2D.Double();
    // refactor #109 variable should be final
    drawQuarters_Loop(g, colors, x, y, halfWidth, halfHeight, offset, rectangle);
    // refactor #7 bad smells long method extraction
  }

  /**
   * Draw quarters loop.
   *
   * @param g the g
   * @param colors the colors
   * @param x the x
   * @param y the y
   * @param halfWidth the half width
   * @param halfHeight the half height
   * @param offset the offset
   * @param rectangle the rectangle
   */
  public void drawQuarters_Loop(Graphics2D g, int colors, int[] x, int[] y, double halfWidth,
      double halfHeight, double offset, Rectangle2D.Double rectangle) {
    for (int index = 0; index < 4; index++) {
      g.setColor(backgroundColors.get(index % colors));
      rectangle.setRect(x[index] * halfWidth - offset, y[index] * halfHeight,
          halfWidth + x[index] * offset, halfHeight);
      g.fill(rectangle);
    }
  }

  /**
   * Draw per bend.
   *
   * @param g the g
   * @param sinister the sinister
   */
  private void drawPerBend(Graphics2D g, boolean sinister) {
    drawBackground(g);
    final int colors = backgroundColors.size();
    // refactor #110 variable should be final
    final GeneralPath path = drawPerBend_GenPath(sinister);
    // refactor #2 bad smells long method extraction
    // refactor #111/ variable should be final
    g.setColor(backgroundColors.get(1 % colors));
    g.fill(path);
  }

  /**
   * Draw per bend gen path.
   *
   * @param sinister the sinister
   * @return the general path
   */
  public GeneralPath drawPerBend_GenPath(boolean sinister) {
    final GeneralPath path = new GeneralPath();
    // refactor #112 variable should be final
    path.moveTo(0, HEIGHT);
    path.lineTo(sinister ? WIDTH : 0, 0);
    path.lineTo(WIDTH, HEIGHT);
    return path;
  }

  /**
   * Draw per saltire.
   *
   * @param g the g
   */
  private void drawPerSaltire(Graphics2D g) {
    final int colors = backgroundColors.size(); // refactor #113 var should be final
    final GeneralPath path = new GeneralPath(); // refactor #114 var should be final
    final int[] x = { 0, WIDTH, WIDTH, 0 }; // refactor #115 var should be final
    final int[] y = { 0, 0, HEIGHT, HEIGHT }; // refactor #116 var should be final
    final double halfWidth = WIDTH / 2; // refactor #117 var should be final
    final double halfHeight = HEIGHT / 2; // refactor #118 var should be final

    drawPerSaltire_Draw(g, colors, path, x, y, halfWidth, halfHeight);
    // refactor #3 bad smells long method extraction
  }

  /**
   * Draw per saltire draw.
   *
   * @param g the g
   * @param colors the colors
   * @param path the path
   * @param x the x
   * @param y the y
   * @param halfWidth the half width
   * @param halfHeight the half height
   */
  public void drawPerSaltire_Draw(Graphics2D g, int colors, GeneralPath path, int[] x, int[] y,
      double halfWidth, double halfHeight) {
    for (int index = 0; index < 4; index++) {
      drawPerSaltire_DrawLines(g, colors, path, x, y, halfWidth, halfHeight, index);
    }
  }

  /**
   * Draw per saltire draw lines.
   *
   * @param g the g
   * @param colors the colors
   * @param path the path
   * @param x the x
   * @param y the y
   * @param halfWidth the half width
   * @param halfHeight the half height
   * @param index the index
   */
  public void drawPerSaltire_DrawLines(Graphics2D g, int colors, GeneralPath path, int[] x, int[] y,
      double halfWidth, double halfHeight, int index) {
    drawPerSaltire_DrawLinesPath(path, x, y, halfWidth, halfHeight, index);
    g.setColor(backgroundColors.get(index % colors));
    g.fill(path);
    path.reset();
  }

  /**
   * Draw per saltire draw lines path.
   *
   * @param path the path
   * @param x the x
   * @param y the y
   * @param halfWidth the half width
   * @param halfHeight the half height
   * @param index the index
   */
  public void drawPerSaltire_DrawLinesPath(GeneralPath path, int[] x, int[] y, double halfWidth,
      double halfHeight, int index) {
    path.moveTo(x[index], y[index]);
    path.lineTo(halfWidth, halfHeight);
    final int nextIndex = (index + 1) % 4;
    // refactor #119 variable should be final
    path.lineTo(x[nextIndex], y[nextIndex]);
  }

  /**
   * Gets the cross based on
   * the decoration.
   *
   * @param decoration the decoration
   * @return the cross
   */
  private GeneralPath getCross(Decoration decoration) {
    final double quarterWidth = (WIDTH - DECORATION_SIZE) / 2;
    // refactor #120 variable should be final
    final double quarterHeight = (HEIGHT - DECORATION_SIZE) / 2;
    // refactor #121 variable should be final
    final double offset = getCross_offset(decoration);
    // refactor #122 variable should be final
    double width = WIDTH;
    double height = HEIGHT;
    switch (decoration) {
      case GREEK_CROSS:
        width = height = Math.min(WIDTH, HEIGHT) - DECORATION_SIZE_DOUBLED;
        // refactor #51 CodePro audit expression evaluation
        // [DECORATION_SIZE * 2 --> DECORATION_SIZE_DOUBLED]
        break;
      default:
        break;
    }
    final GeneralPath cross = new GeneralPath();
    // refactor #123 variableshould be final
    cross.append(new Rectangle2D.Double((WIDTH - width) / 2, quarterHeight, width, DECORATION_SIZE),
        false);
    cross.append(new Rectangle2D.Double(quarterWidth - offset, (HEIGHT - height) / 2,
        DECORATION_SIZE, height), false);
    return cross;
  }

  /**
   * Gets the cross offset based
   * on the decoration used.
   *
   * @param decoration the decoration
   * @return the cross offset
   */
  private double getCross_offset(Flag.Decoration decoration) {
    double offset = 0;
    if (decoration == Decoration.SCANDINAVIAN_CROSS) {
      // refactor #157 Checkstyle 'if' must use {}'s
      offset = CROSS_OFFSET;
    }
    return offset;
  }

  /**
   * Gets the bend.
   *
   * @param sinister the sinister
   * @return the bend
   */
  private GeneralPath getBend(boolean sinister) {
    final GeneralPath path = new GeneralPath();
    // refactor #124 variable should be final
    if (sinister) {
      path.moveTo(0, HEIGHT);
      path.lineTo(0, HEIGHT - BEND_Y);
      path.lineTo(WIDTH - BEND_X, 0);
      path.lineTo(WIDTH, 0);
      path.lineTo(WIDTH, BEND_Y);
      path.lineTo(BEND_X, HEIGHT);
    } else {
      path.moveTo(0, 0);
      path.lineTo(BEND_X, 0);
      path.lineTo(WIDTH, HEIGHT - BEND_Y);
      path.lineTo(WIDTH, HEIGHT);
      path.lineTo(WIDTH - BEND_X, HEIGHT);
      path.lineTo(0, BEND_Y);
    }
    return path;
  }

  /**
   * Gets the pall.
   *
   * @return the pall
   */
  private GeneralPath getPall() {
    final double y1 = (HEIGHT - DECORATION_SIZE) / 2;
    // refactor #125 variable should be final
    final double y2 = (HEIGHT + DECORATION_SIZE) / 2;
    // refactor #126 variable should be final
    final double x = BEND_X + y1 * SQRT_3;
    // refactor #127 variable should be final
    final GeneralPath path = new GeneralPath(getTriangle(UnionShape.CHEVRON, true));
    // refactor #128 variable should be final
    path.lineTo(0, HEIGHT);
    path.lineTo(BEND_X, HEIGHT);
    path.lineTo(x, y2);
    path.lineTo(WIDTH, y2);
    path.lineTo(WIDTH, y1);
    path.lineTo(x, y1);
    path.lineTo(BEND_X, 0);
    path.lineTo(0, 0);
    return path;
  }

  /**
   * Returns a triangle of the given shape and size. This is a large top left triangle if the given
   * shape is BEND, and a small left triangle if the given shape is CHEVRON or TRIANGLE.
   *
   * @param unionShape The shape of the union.
   * @param small      Whether the shape is limited by decorations.
   * @return the triangle
   */
  private GeneralPath getTriangle(UnionShape unionShape, boolean small) {
    final GeneralPath path = new GeneralPath();
    // refactor #129 variable should be final
    final double x = (small) ? BEND_X : 0;
    // refactor #10 combined lines into Ternary Operator conditional,
    // refactor #130 variable should be final
    final double y = (small) ? BEND_Y : 0;
    // refactor #131 variable should be final
    /*
     * //old code double x = 0; double y = 0; if (small) { x = BEND_X; y = BEND_Y; }
     */
    switch (unionShape) {
      case BEND:
        path.moveTo(0, HEIGHT - y);
        path.lineTo(0, 0);
        path.lineTo(WIDTH - x, 0);
        break;
      case CHEVRON:
        path.moveTo(0, y);
        path.lineTo(CHEVRON_X - x, HEIGHT_HALF);
        // refactor #52 CodePro audit expression evaluation
        path.lineTo(0, HEIGHT - y);
        break;
      case TRIANGLE:
        if (unionPosition == UnionPosition.LEFT || unionPosition == UnionPosition.RIGHT) {
          path.moveTo(0, y);
          path.lineTo(WIDTH_HALF - x, HEIGHT_HALF);
          // refactor #53 CodePro audit expression evaluation
          path.lineTo(0, HEIGHT - y);
        } else {
          path.moveTo(0, x);
          path.lineTo(HEIGHT_HALF - y, WIDTH_HALF);
          // refactor #54 CodePro audit expression evaluation
          path.lineTo(0, WIDTH - x);
        }
        break;
      default:
        break;
    }
    return path;
  }

  /**
   * Gets the rhombus lines and
   * pre-draws them.
   *
   * @return the drawn rhombus
   */
  private GeneralPath getRhombus() {
    final GeneralPath rhombus = new GeneralPath();
    // refactor #132 variable should be final
    rhombus.moveTo(WIDTH_HALF, BEND_Y);
    // refactor #55 CodePro audit expression evaluation
    rhombus.lineTo(WIDTH - BEND_X, HEIGHT_HALF);
    // refactor #56 CodePro audit expression evaluation
    rhombus.lineTo(WIDTH_HALF, HEIGHT - BEND_Y);
    // refactor #57 CodePro audit expression evaluation
    rhombus.lineTo(BEND_X, HEIGHT_HALF);
    // refactor #58 CodePro audit expression evaluation
    return rhombus;
  }

  /**
   * Gets the rectangle.
   *
   * @return the rectangle
   */
  private Rectangle2D.Double getRectangle() {
    if (unionPosition == null || unionPosition == UnionPosition.NONE) {
      return null;
    }
    final Rectangle2D.Double union = new Rectangle2D.Double();
    // refactor #133 variable should be final
    if (unionPosition.alignment == Alignment.VERTICAL) {
      if (background.alignment == Alignment.VERTICAL && stripes < 3) {
        union.width = WIDTH / stripes;
        if (stripes == 2 && unionPosition == UnionPosition.RIGHT) {
          union.x = WIDTH_HALF;
          // refactor #59 CodePro audit expression evaluation
        }
      } else {
        union.width = WIDTH / 3;
        union.x = unionPosition.index * union.width;
      }
      union.height = HEIGHT;
    } else if (unionPosition.alignment == Alignment.HORIZONTAL) {
      if (background.alignment == Alignment.HORIZONTAL && stripes < 3) {
        union.height = HEIGHT / stripes;
        if (stripes == 2 && unionPosition == UnionPosition.BOTTOM) {
          union.y = HEIGHT_HALF;
          // refactor #60 CodePro audit expression evaluation
        }
      } else {
        union.height = HEIGHT / 3;
        union.y = unionPosition.index * union.height;
      }
      union.width = WIDTH;
    } else {
      // canton
      union.width = WIDTH_HALF; // refactor #61 CodePro audit expression evaluation
      union.height = HEIGHT_HALF; // refactor #62 CodePro audit expression evaluation
      if (background.alignment == Alignment.HORIZONTAL) {
        union.height = (stripes < 3) ? HEIGHT_HALF
            : (stripes / 2) * getStripeHeight(background.alignment);
        // refactor #63 CodePro audit expression evaluation
      } else if (background.alignment == Alignment.VERTICAL) {
        union.width = (stripes < 3) ? WIDTH_HALF
            : (stripes / 2) * getStripeWidth(background.alignment);
        // refactor #64 CodePro audit expression evaluation
      }
      if (decoration == Decoration.GREEK_CROSS || decoration == Decoration.CROSS) {
        union.width = (WIDTH - DECORATION_SIZE) / 2;
        union.height = (HEIGHT - DECORATION_SIZE) / 2;
      } else if (decoration == Decoration.SCANDINAVIAN_CROSS) {
        union.width = (WIDTH - DECORATION_SIZE) / 2 - CROSS_OFFSET;
        union.height = (HEIGHT - DECORATION_SIZE) / 2;
      }
    }

    return union;
  }

  /**
   * Return the basic five-point star.
   *
   * @return the basic star shape
   */
  private static GeneralPath getStar() {
    final GeneralPath star = new GeneralPath(GeneralPath.WIND_NON_ZERO);
    // refactor #134 variable should be final
    final double angle = 2 * Math.PI / 5; // refactor #135 variable should be final
    double radius = STAR_SIZE / 2;
    double x = 0;
    double y = -radius;
    star.moveTo(x, y);
    final int[] vertex = { 2, 4, 1, 3 }; // refactor #136 variable should be final
    for (int i : vertex) {
      x = getStar_x(angle, radius, x, i);
      // refactor #22 extracted long method bad small
      y = getStar_y(angle, radius, y, i);
      // refactor #23 extracted long method bad small
      star.lineTo(x, y);
    }
    star.closePath();
    return star;
  }

  /**
   * Gets the star y.
   *
   * @param angle the angle
   * @param radius the radius
   * @param y the y
   * @param i the i
   * @return the star y
   */
  private static double getStar_y(double angle, double radius, double y, int i) {
    final double phi = i * angle; // refactor #137 variable should be final
    y = -radius * Math.cos(phi);
    return y;
  }

  /**
   * Gets the star x.
   *
   * @param angle the angle
   * @param radius the radius
   * @param x the x
   * @param i the i
   * @return the star x
   */
  private static double getStar_x(double angle, double radius, double x, int i) {
    final double phi = i * angle; // refactor #138 variable should be final
    x = radius * Math.sin(phi);
    return x;
  }

  /**
   * Returns a star at the given coordinates (x, y).
   *
   * @param x The x coordinate of the star.
   * @param y The y coordinate of the star.
   * @return the star
   */
  public GeneralPath getStar(double x, double y) {
    return getStar(-1, x, y);
  }

  /**
   * Returns a star of the given scale at the given coordinates (x, y).
   *
   * @param scale The scale of the star.
   * @param x     The x coordinate of the star.
   * @param y     The y coordinate of the star.
   * @return the star
   */
  public GeneralPath getStar(double scale, double x, double y) {
    final GeneralPath newStar = new GeneralPath(star);
    // refactor #139 variable should be final
    if (scale > 0) {
      newStar.transform(AffineTransform.getScaleInstance(scale, scale));
    }
    newStar.transform(AffineTransform.getTranslateInstance(x, y));
    return newStar;
  }

  /**
   * Centers the given path on the given point (x,y).
   *
   * @param path The path to center.
   * @param x    The x coordinate of the center.
   * @param y    The y coordinate of the center.
   */
  private void center(GeneralPath path, double x, double y) {
    final double dx = x - path.getBounds().getX() - path.getBounds().getWidth() / 2;
    // refactor #140 variable should be final
    final double dy = y - path.getBounds().getY() - path.getBounds().getHeight() / 2;
    // refactor #141 variable should be final
    path.transform(AffineTransform.getTranslateInstance(dx, dy));
  }

  /**
   * Returns either a single star, or a circle of stars with the given radius, centered at the
   * origin.
   *
   * @param radius The radius of the circle.
   * @return The circle of stars.
   */
  private GeneralPath getCircleOfStars(double radius) {
    final double phi = Math.PI * 2 / stars;
    // refactor #142 variable should be final
    GeneralPath unionPath = new GeneralPath();
    if (stars == 0) {
      // nothing to do
    } else if (stars == 1) {
      // one double sized star
      unionPath = getStar(2, 0, 0);
    } else if (stars == 2) {
      // two larger stars, on the x axis
      unionPath.append(getStar(1.5, -radius, 0), false);
      unionPath.append(getStar(1.5, radius, 0), false);
    } else {
      // a general circle of stars
      for (int i = 0; i < stars; i++) {
        double x = -radius - radius * Math.sin(i * phi);
        double y = -radius * Math.cos(i * phi);
        unionPath.append(getStar(x, y), false);
      }
    }
    return unionPath;
  }

  /**
   * Gets the grid of stars
   * for the flag creation.
   *
   * @param union the union type
   * @return the grid number of stars
   */
  public GeneralPath getGridOfStars(Rectangle2D.Double union) {
    final int[] bars = getGridOfStars_Bars();
    // Refactor #4 -- bad smell long method extracted,
    // refactor #145 variable should be final
    final int maxCols = Math.max(bars[0], bars[1]);
    // refactor #146 variable should be final
    final int rows = getGridOfStars_Sum(bars);
    // Refactor #5 -- bad smell long method extracted,
    // refactor #147 variable should be final
    final double hSpace = union.getWidth() / (2 * maxCols);
    // refactor #143 variable should be final
    final double vSpace = union.getHeight() / (2 * rows);
    // refactor #144 variable should be final
    double y = 0;
    final GeneralPath grid = new GeneralPath();
    // refactor #148 variable should be final
    int count = 1;
    for (int row = 0; row < rows; row++) {
      int cols = bars[row % 2];
      double x = (cols == maxCols) ? 0 : hSpace;
      for (int col = 0; col < cols; col++) {
        if (count > stars) {
          break;
        }
        grid.append(getStar(x, y), false);
        x += 2 * hSpace;
        count++;
      }
      y += 2 * vSpace;
    }
    return grid;
  }

  /**
   * Gets the grid of stars number
   * of bars.
   *
   * @return the grid of stars bars
   */
  public int[] getGridOfStars_Bars() {
    int[] bars = new int[2];
    for (int count = stars; count < 51; count++) {
      if (layout[count][0] > 0) {
        bars = layout[count];
        break;
      }
    }
    return bars;
  }

  /**
   * Gets the grid of stars sum
   * for the gridOfStars method
   *
   * @param bars the number of bars
   * @return the grid of stars sum
   */
  public int getGridOfStars_Sum(int[] bars) {
    int rows = 2;
    int sum = bars[0] + bars[1];
    while (sum < stars) {
      sum += bars[rows % 2];
      rows++;
    }
    return rows;
  }

}
