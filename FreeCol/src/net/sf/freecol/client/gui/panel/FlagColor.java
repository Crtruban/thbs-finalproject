/**
 * Copyright (C) 2002-2015   The FreeCol Team This file is part of FreeCol. FreeCol is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version. FreeCol is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with FreeCol.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.sf.freecol.client.gui.panel;


import java.awt.Color;

public class FlagColor {
	private Color unionColor = Color.BLUE;
	private Color starColor = Color.WHITE;
	private Color decorationColor = Color.WHITE;

	public Color getUnionColor() {
		return unionColor;
	}

	public Color getStarColor() {
		return starColor;
	}

	public Color getDecorationColor() {
		return decorationColor;
	}

	/**
	* Sets the union color of the flag and returns the flag itself.
	* @param unionColor  The new <code>Color</code> value.
	* @return  The modified flag.
	*/
	public Flag setUnionColor(Color unionColor, Flag flag) {
		this.unionColor = unionColor;
		return flag;
	}

	/**
	* Sets the star color of the flag and returns the flag itself.
	* @param starColor  The new <code>Color</code> value.
	* @return  The modified flag.
	*/
	public Flag setStarColor(Color starColor, Flag flag) {
		this.starColor = starColor;
		return flag;
	}

	/**
	* Sets the decoration color of the flag and returns the flag itself.
	* @param decorationColor  The new <code>Color</code> value.
	* @return  The modified flag.
	*/
	public Flag setDecorationColor(Color decorationColor, Flag flag) {
		this.decorationColor = decorationColor;
		return flag;
	}
}